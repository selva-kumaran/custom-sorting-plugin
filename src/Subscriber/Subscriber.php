<?php declare(strict_types=1);

namespace CustomSorting\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Storefront\Page\Wishlist\WishListPageProductCriteriaEvent;
class Subscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            WishListPageProductCriteriaEvent::class => 'onWishListPageProductCriteriaEvent',
        ];
    }
    public function onWishListPageProductCriteriaEvent(WishListPageProductCriteriaEvent $event)
    {
        $event->getCriteria()->addSorting(new FieldSorting('wishlists.createdAt', FieldSorting::ASCENDING));
    }
}
